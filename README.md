# scandiweb php task

Basic product list for showing our different types of products (DVD , Book , Furniture)
The project based on the laravel framework.

-----
## Table of Contents

* [Features](#item1)
* [Requirements](#item2)
* [Installation](#item3)
* [Tools](#item4)
* [Demo](#item5)

-----
<a name="item1"></a>
## Features:
  * List all different products.
  * Add new product with different type based on type switcher.
  * Remove multiple products using checkbox and mass delete button
  

-----
<a name="item2"></a>
## Requirements:
  * PHP 7.3+
  * Apache (XAMPP)
  * Composer
  

-----
<a name="item3"></a>
## Installation:

Clone this repository and install the dependencies.

		$ git clone https://almashad@bitbucket.org/almashad/product-list.git
		$ composer install

Rename .env.example to .env then open change

		DB_DATABASE=your database name  
		DB_USERNAME=username
		DB_PASSWORD=password
	
Run the Migration to populate the database and tables

   		$ php artisan migrate --seed
	
Finally, serve the application.

    	$ php artisan serve
-----

<a name="item4"></a>
## Tools:
  * Laravel 8
  * MySQL database
  * HTML
  * CSS
  * jQuery
  
  

-----

<a name="item5"></a>
## Demo:
<a href="https://amazingproductsfoamazingscandi.000webhostapp.com/" target="_blank">Product List</a>

-----


