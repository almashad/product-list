@extends('layouts.app')
@section('content')
    <div class="container">
        <a href="{{ url('products/add') }}" class="btn btn-primary">ADD </a>
        <form method="post" action="{{url('products/mass-delete')}}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="delete">
            <input id="delete-product-btn" class="btn btn-danger" type="submit" name="submit" value="MASS DELETE !"/>
            <div class="row">
            @foreach ($products as $product)

                <?php $productType = App\Models\ProductType::where('id', $product->type_id)->first();  ?> 
                <?php $productAttribute = App\Models\productAttribute::where('product_id', $product->id)->first();  ?> 
                <?php $attribute = App\Models\Attribute::where('id', $productAttribute->attribute_id)->first();  ?> 
                <div class="col-md-2 product-item" >
                    <input type="checkbox" name="product[]" class="delete-checkbox" value="{{ $product->id }}" />
                    <p>Name  : {{$product->name}}</p>
                    <p>Price : {{$product->price }} {{ '$'}}</p>
                    <p>Type  : {{$productType->name}} </p>
                    <p>{{$attribute->name}} {{$productAttribute->value}} {{$attribute->measurement}} </p>
                </div>
            @endforeach
            </div>
        </form>    
    </div>
@endsection

<style>
    .product-item{
        border: 1px solid;
        margin:10px;
    }
    .actions {
        margin-left: 895px;
    }
    #delete-product-btn
    {
        margin-left: 880px;
    }
</style>