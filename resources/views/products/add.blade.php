@extends('layouts.app')

@section('content')
<div class="container">
<form id="product-form" method="post" action="/products/store">
    {{ csrf_field() }}
    <div class="form-group row">
        <label for="sku" class="col-sm-3 col-form-label">sku</label>
        <div class="col-sm-9">
            <input name="sku" type="text" class="form-control" id="sku" placeholder="sku" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="product_name" class="col-sm-3 col-form-label">Name</label>
        <div class="col-sm-9">
            <input name="product_name" type="text" class="form-control" id="product_name"
                   placeholder="name" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="price" class="col-sm-3 col-form-label">Price</label>
        <div class="col-sm-9">
            <input name="price" type="number" class="form-control" id="price"
                   placeholder="Price" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="type" class="col-sm-3 col-form-label">Type</label>
        <div class="col-sm-9">
        <select class="form-select" name="type" id="type">
            <option selected>Select the Type</option>
            <option value="dvd">DVD</option>
            <option value="book">Book</option>
            <option value="furniture">Furniture</option>
          </select>
        </div>
    </div>
    <!-- DVD-disc size -->
    <div class="form-group row no_display"  id="dvd_section">
        <label for="size" class="col-sm-3 col-form-label">Size</label>
        <div class="col-sm-9">
            <input name="size" type="number" class="form-control" id="size"
                   placeholder="size"  >
        </div>
    </div>
    <div class="form-group row no_display"  id="book_section">
        <label for="weight" class="col-sm-3 col-form-label">Weight</label>
        <div class="col-sm-9">
            <input name="weight" type="number" class="form-control" id="weight"
                   placeholder="weight"  >
        </div>
    </div>
    <div class="form-group row no_display"  id="furniture_section">
        <label for="furniture_dimensions" class="col-sm-3 col-form-label">Dimensions</label>
        <div class="col-sm-9">
            <table class="table">
                <tr>
                    <td>Height </td>
                    <td><input type="number" step="0.01" class="form-control" id="height" name="height" /></td>
                </tr>
                <tr>
                    <td>Width </td>
                    <td><input type="number" step="0.01" class="form-control" id="width" name="width"  /></td>
                </tr>
                <tr>
                    <td>Length </td>
                    <td><input type="number" step="0.01" class="form-control" id="length" name="length"  /></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="form-group row">
        <div class="offset-sm-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
            <a href="{{ url('products') }}" class="btn btn-danger">Cancel </a>
    </div>
</form>
</div>
<style>
    .no_display {display: none}
    #product_form{
  margin-left: 10px;
    }
}
</style>
@endsection
