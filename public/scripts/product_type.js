$(document).ready(function(){
    $('#type').change(function(i){
        if (this.value == "dvd")
        {
            $('#dvd_section').removeClass('no_display');
            $("size").attr("required", true);
            $('#book_section').addClass('no_display').prop('required',false);
            $('#furniture_section').addClass('no_display').prop('required',false);


            $('#size').prop('required',true);
            $('#weight').prop('required',false);
            
            $('#height').prop('required',false);
            $('#width').prop('required',false);
            $('#length').prop('required',false);
        }
        else if (this.value == "book")
        {
            $('#dvd_section').addClass('no_display').prop('required',false);
            $('#book_section').removeClass('no_display');
            $('#furniture_section').addClass('no_display').prop('required',false);


            $('#weight').prop('required',true);

            $('#size').prop('required',false);
            $('#width').prop('required',false);
            $('#height').prop('required',false);
            $('#length').prop('required',false);
        }
        else if (this.value == "furniture")
        {
            $('#book_section').addClass('no_display').prop('required',false);
            $('#dvd_section').addClass('no_display').prop('required',false);
            $('#furniture_section').removeClass('no_display').prop('required',true);


            $('#width').prop('required',true);
            $('#height').prop('required',true);
            $('#length').prop('required',true);
            $('#size').prop('required',false);
            $('#weight').prop('required',false);
       
        }
        else
        {
            $('#book_section').addClass('no_display');
            $('#dvd_section').addClass('no_display');
            $('#furniture_section').addClass('no_display');

            $('#size').prop('required',false);
            $('#weight').prop('required',false);
            $('#height').prop('required',false);
            $('#width').prop('required',false);
            $('#length').prop('required',false);
        }
    });
});