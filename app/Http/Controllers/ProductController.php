<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Attribute;
use App\Models\ProductType;
use App\Models\ProductAttribute;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::with('productType', 'ProductAttribute')->get();
        return view('products.products')->with('products', $products);
    }

    public function add()
    {
        return view('products.add');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'sku' => 'required',
            'product_name' => 'required',
            'price' => 'required',
            'type' => 'required'
        ]);
        $product = new Product();
        $product->sku = $request->sku;
        $product->name = $request->product_name;
        $product->price = $request->price;
        $type = $request->type;
        $product_type = ProductType::where('name', $type)->first();
        if (!is_null($product_type)) {
            $product->type_id = $product_type->id;
            $product->save();
            $product_attribute = new ProductAttribute();
            switch ($type) {
                case 'dvd':
                    $attribute = Attribute::where('name', 'size')->first();
                    $product_attribute->value = $request->size;
                    $product_attribute->product_id = $product->id;
                    $product_attribute->attribute_id = $attribute->id;

                    break;
                case 'book':
                    $attribute = Attribute::where('name', 'weight')->first();
                    $product_attribute->value = $request->weight;
                    $product_attribute->product_id = $product->id;
                    $product_attribute->attribute_id = $attribute->id;

                    break;
                default:
                    $attribute = Attribute::where('name', 'Dimensions')->first();
                    $product_attribute->value = $request->length . "x" . $request->width . "x" . $request->height;
                    $product_attribute->product_id = $product->id;
                    $product_attribute->attribute_id = $attribute->id;

                    break;

            }
            $product_attribute->save();
        }
        return redirect()->route('index')->with('success','product created successfully!');
    }
    public function deleteAll(Request $request)
    {

        $data = $request->all();
        foreach ($data['product'] as $i => $id) {

            $productAttribute = ProductAttribute::where('product_id',$id);
            $deleted = $productAttribute->delete();
            if ($deleted) {
                $product = Product::find($id);
                $product->delete();
                if($deleted)
                {
                    return redirect()->route('index')->with('success','products deleted successfully!');
                }
                else
                {
                    return redirect()->route('index')->with('error','Error in deleting products');     
                }
            }
        }

    }
}
