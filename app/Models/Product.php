<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Attribute;

class Product extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'sku',
        'price',
        'type'
    ];
    protected $hidden = [

    ];


    public function productType()
    {
        return $this->belongsTo(ProductType::class);
    }

    public function Attribute()
    {
        return $this->belongsTo(Attribute::class);
    }
    public function productAttribute()
    {
        return $this->belongsTo(ProductAttribute::class);
    }
}
